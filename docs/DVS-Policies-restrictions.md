# DVS Richtlinien

| Rule/Policy | Hinweis/Einschränkung |
| ------ | ------ |
| deny-privilege-escalation | keine Einschränkung |
| disallow-add-capabilities | keine Einschränkung |
| disallow-default-serviceaccount |keine Einschränkung |
| disallow-host-namespaces | keine Einschränkung |
| disallow-host-path | keine Einschränkung |
| disallow-host-ports | keine Einschränkung |
| disallow-latest-tag |keine Einschränkung |
| disallow-privileged-containers | keine Einschränkung |
| disallow-selinux-options | keine Einschränkung |
| imagepullpolicy-always | keine Einschränkung |
| require-default-proc-mount | keine Einschränkung |
| require-gid-greater-2000 | keine Einschränkung |
| require-health-and-liveness-check | keine Einschränkung |
| require-limits-and-requests |keine Einschränkung |
| require_ro_rootfs | keine Einschränkung |
| require-run-as-non-root | keine Einschränkung |
| require-uid-greater-2000 |  keine Einschränkung |
| require-unique-uid-per-workload | keine Einschränkung |
| restrict-apparmor | keine Einschränkung |
| restrict-external-ips | keine Einschränkung |
| restrict-image-registries |keine Einschränkung |
| restrict-sysctls | keine Einschränkung |
