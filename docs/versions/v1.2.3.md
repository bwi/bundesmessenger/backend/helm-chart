# Abhängigkeiten BundesMessenger Helm Chart 1.2.3 (2023-06-26)

## Container Images

| Name | Version | Image | Tag |
|---------|---------|---------|---------|
| Basis | 22.04 | registry.opencode.de/bwi/bundesmessenger/backend/container-images/ubuntu | latest-jammy-production |
| Webclient | 2.4.0 | registry.opencode.de/bwi/bundesmessenger/backend/container-images/bundesmessenger-web | 2.4.0-jammy-production |
| ClamAV | 0.103.8 | registry.opencode.de/bwi/bundesmessenger/backend/container-images/clamav | 0.103.8_dfsg-0ubuntu0.22.04.1-jammy-production |
| cicap | 0.5.6 | registry.opencode.de/bwi/bundesmessenger/backend/container-images/icap | 1_0.5.6-2build1-jammy-production |
| Matrix-Content-Scanner | 1.0.3 | registry.opencode.de/bwi/bundesmessenger/backend/container-images/matrix-content-scanner | 1.0.3-jammy-production |
| Kubectl | 1.25.4 | registry.opencode.de/bwi/bundesmessenger/backend/container-images/kubectl | 1.25.4-jammy-production |
| Nginx | 1.18.0 | registry.opencode.de/bwi/bundesmessenger/backend/container-images/nginx | 1.18.0-6ubuntu14.3-jammy-production |
| Redis | 6.0.16 | registry.opencode.de/bwi/bundesmessenger/backend/container-images/redis | 5_6.0.16-1ubuntu1-jammy-production |
| Sygnal | 0.12.0 | registry.opencode.de/bwi/bundesmessenger/backend/container-images/sygnal | 0.12.0-jammy-production |
| Synapse | 1.78.0 | registry.opencode.de/bwi/bundesmessenger/backend/container-images/synapse | 1.78.0-jammy-production |
| Synapse-Admin | 0.8.7 | registry.opencode.de/bwi/bundesmessenger/backend/container-images/synapse-admin | 0.8.7-jammy-production |
| Connection-Tests | 1.21.2 | registry.opencode.de/bwi/bundesmessenger/backend/container-images/wget | 1.21.2-jammy-production |
| internalPostgresql |  | registry.opencode.de/ig-bvc/demo-apps/postgresql/postgres | 14 |

## Helm Charts

| Name | Version | Repository |
|---------|---------|---------|
| redis | ^17.3.17 | https://charts.bitnami.com/bitnami |
| postgresql | ^12.1.6 | https://charts.bitnami.com/bitnami |
