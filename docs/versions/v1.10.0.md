# Abhängigkeiten BundesMessenger Helm Chart 1.10.0 (2024-11-05)

## Container Images

| Name | Image | Tag |
|---------|---------|---------|
| Basis | registry.opencode.de/bwi/bundesmessenger/backend/container-images/ubuntu | latest-jammy-production |
| Webclient | registry.opencode.de/bwi/bundesmessenger/backend/container-images/bundesmessenger-web | 2.21.0-jammy-production |
| Call | registry.opencode.de/bwi/bundesmessenger/backend/container-images/bundesmessenger-call | 0.4.0-jammy-production |
| LiveKit-JWT-Service | ghcr.io/element-hq/lk-jwt-service | sha-e3a1a01 |
| ClamAV | registry.opencode.de/bwi/bundesmessenger/backend/container-images/clamav | 0.103.9_dfsg-0ubuntu0.22.04.1-jammy-production |
| cicap | registry.opencode.de/bwi/bundesmessenger/backend/container-images/icap | 1_0.5.6-2build1-jammy-production |
| Matrix-Content-Scanner | registry.opencode.de/bwi/bundesmessenger/backend/container-images/matrix-content-scanner | 1.1.0-jammy-production |
| Synapse | registry.opencode.de/bwi/bundesmessenger/backend/container-images/synapse | 1.105.1-jammy-production |
| Kubectl | registry.opencode.de/bwi/bundesmessenger/backend/container-images/kubectl | 1.31.2-jammy-production |
| Nginx | registry.opencode.de/bwi/bundesmessenger/backend/container-images/nginx | 1.18.0-6ubuntu14.4-jammy-production |
| Redis | registry.opencode.de/bwi/bundesmessenger/backend/container-images/redis | 5_6.0.16-1ubuntu1-jammy-production |
| Sygnal | registry.opencode.de/bwi/bundesmessenger/backend/container-images/sygnal | 0.15.1-jammy-production |
| Synapse-Admin | registry.opencode.de/bwi/bundesmessenger/backend/container-images/synapse-admin | 0.9.1-jammy-production |
| Matrix-Authentication-Service | ghcr.io/element-hq/matrix-authentication-service | 0.12.0-debug |
| internalPostgresql | docker.io/bitnami/postgresql | 16 |
| Connection-Tests | registry.opencode.de/bwi/bundesmessenger/backend/container-images/wget | 1.21.2-jammy-production |

## Helm Charts

| Name | Version | Repository |
|---------|---------|---------|
| redis | ^17.14.6 | oci://registry-1.docker.io/bitnamicharts |
| postgresql | ^15.5 | oci://registry-1.docker.io/bitnamicharts |
