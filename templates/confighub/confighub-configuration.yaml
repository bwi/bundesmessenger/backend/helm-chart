{{/* SPDX-FileCopyrightText: 2023–2024 BWI GmbH */}}
{{/* SPDX-License-Identifier: Apache-2.0 */}}
{{- if .Values.confighub.enabled }}
{{- $name := include "matrix-synapse.externalname" (dict "global" . "external" "confighub-nginx") -}}
{{- $component := "confighub-nginx" -}}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ $name }}
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "matrix-synapse.labels" . | nindent 4 }}
    component: {{ $component }}
data:
  confighub.conf: |
    server {
      listen 8080 default_server;
      {{- if not .Values.ipv4Only }}
      listen [::]:8080 default_server;
      {{- end }}
      server_name _;

      root {{ .Values.wellknown.htdocsPath }};
      # livenessProbe auf /status, logging deaktiviert
      location /status {
        return 200;
        access_log off;
      }
      location /_matrix/cmaintenance {
        # X-Header for CORS-Policy
        include /etc/nginx/conf.d/cors_headers.conf;
        add_header Access-Control-Allow-Headers "Origin, X-Requested-With, Content-Type, Accept, Authorization";
        add_header Access-Control-Allow-Methods "GET, POST, PUT, DELETE, OPTIONS";
        add_header Access-Control-Allow-Origin "*";
        add_header Content-Type application/json;
        rewrite ^/_matrix/cmaintenance /maintenance.json break;
      }
      location /_matrix/cconfig/style.json {
        # X-Header for CORS-Policy
        include /etc/nginx/conf.d/cors_headers.conf;
        add_header Access-Control-Allow-Headers "Origin, X-Requested-With, Content-Type, Accept, Authorization";
        add_header Access-Control-Allow-Methods "GET, POST, PUT, DELETE, OPTIONS";
        add_header Access-Control-Allow-Origin "*";
        add_header Content-Type application/json;
        rewrite ^/_matrix/cconfig/style.json /style.json break;
      }
      location /_bum/client/v1/verify {
        include /etc/nginx/conf.d/cors_headers.conf;
        add_header Content-Type application/json;
        return 200 '
          {{- toPrettyJson ( .Values.additionalConfig.verifyJWT | default list ) | nindent 8 }}
        ';
      }
    }
  style.json: |
    {{- if .Values.additionalConfig.locationSharing.map_style_config }}
{{ toRawJson .Values.additionalConfig.locationSharing.map_style_config | indent 4 }}
    {{- else }}
{{ printf "{}" | indent 4 }}
    {{- end }}
  maintenance.json: |
    {{- if .Values.additionalConfig.maintenance }}
{{ toPrettyJson .Values.additionalConfig.maintenance | indent 4 }}
    {{- else }}
{{ printf "{}" | indent 4 }}
    {{- end }}


{{- end -}}
