{{/* SPDX-FileCopyrightText: 2023–2025 BWI GmbH */}}
{{/* SPDX-License-Identifier: Apache-2.0 */}}
{{- if and ( .Values.demomode.enabled | default false ) (semverCompare ">=1.21.0" .Capabilities.KubeVersion.Version) -}}
{{- $name := include "matrix-synapse.externalname" (dict "global" . "external" "demomode") -}}

{{ if .Values.demomode.serviceAccount.create }}
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ include "matrix-synapse.demomodeServiceAccountName" . }}
  namespace: {{ .Release.Namespace }}
  labels:
  {{- include "matrix-synapse.labels" . | nindent 4 }}
    app.kubernetes.io/component: deployment-restarter

---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: {{ include "matrix-synapse.demomodeServiceAccountName" . }}
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "matrix-synapse.labels" . | nindent 4 }}
rules:
- apiGroups: ["apps", ""]
  resources: ["deployments", "deployments/scale", "statefulsets", "statefulsets/scale"]
  verbs: ["get", "update", "patch", "scale"]
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["get", "list", "watch"]
- apiGroups: [""]
  resources: ["pods/exec"]
  verbs: ["create"]
- apiGroups: [""]
  resources: ["configMap"]
  verbs: ["get", "update", "patch"]

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: {{ include "matrix-synapse.demomodeServiceAccountName" . }}
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "matrix-synapse.labels" . | nindent 4 }}
subjects:
- kind: ServiceAccount
  name: {{ include "matrix-synapse.demomodeServiceAccountName" . }}
  namespace: {{ .Release.Namespace }}
roleRef:
  kind: Role
  name: {{ include "matrix-synapse.demomodeServiceAccountName" . }}
  apiGroup: rbac.authorization.k8s.io
{{ end }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ $name }}-script
  namespace: {{ .Release.Namespace }}
data:
  check-deployment.sh: |-
    #!/bin/bash
    # wait until deployment really ready
    check_deployment() {
    set +e
    echo "Checking for deployment state..."
    key="$(kubectl -n {{ .Release.Namespace }} get deployment/{{ include "matrix-synapse.fullname" . }} -o=jsonpath='{.status.replicas}' 2> /dev/null)"
    [ $? -ne 0 ] && return 1
    [ -z "$key" ] && return 1
    [ "$key" == "1" ] && return 0
    }

    echo "Waiting for deployment startup."
    while true; do
      if check_deployment ; then
        break
      fi
      echo -n "."
      sleep 10
    done

    # check deployment
    while true; do
      STATUS=$(kubectl -n {{ .Release.Namespace }} get deployment/{{ include "matrix-synapse.fullname" . }} -o=jsonpath='{.status.conditions[?(@.type=="Available")].status}')
      if [[ $STATUS == "True" ]]; then
         echo "Deployment {{ .Release.Name }} of bundesmessenger is running"
         break
      fi
      echo "Deployment {{ .Release.Name }} of bundesmessenger is not running yet"
      sleep 10
    done

    while true; do
      if check_deployment; then
        break
      fi
    done
    # next stage set kind of demo
{{- if eq .Values.demomode.mode "defined" }}
    echo "DB-Restore done"
{{- else if eq .Values.demomode.mode "federation" }}
    echo "TBD"
{{- else }}
{{- /* fallback auf complete */}}
    POD_NAME=$(kubectl get pods --namespace {{ .Release.Namespace }} -l "app.kubernetes.io/name={{ include "matrix-synapse.name" . }},app.kubernetes.io/instance={{ .Release.Name }},app.kubernetes.io/component=synapse" -o jsonpath="{.items[0].metadata.name}")
    kubectl exec --namespace {{ .Release.Namespace }} $POD_NAME -- /script/set-demo-complete-script.sh
{{- end }}
  set-demo-complete-script.sh: |-
    #!/bin/bash
    # Variablen definieren
    if [ -n "ADMINS" ]; then
    ADMIN_USER=$ADMINS
    else
    ADMIN_USER="testadmin"
    fi
    ADMIN_PASSWORD="testadmin"

    # Initialen Administrator erstellen
    for ADMIN in $ADMIN_USER; do
      echo "Creating initial administrator $ADMIN"
      register_new_matrix_user -c /synapse/config/homeserver.yaml -c /synapse/config/conf.d/secrets.yaml -u $ADMIN -p $ADMIN_PASSWORD --admin http://localhost:8008
    done

    if [ -z "USERS" ]; then
    USERS="Max.Mustermann Erika.Mustermann John.Doe"
    fi;
    # Initiale Nutzer erstellen
    echo "Creating initial users..."
    for USER in $USERS; do
      echo "Register user $USER"
      register_new_matrix_user -c /synapse/config/homeserver.yaml -c /synapse/config/conf.d/secrets.yaml -u $USER -p 'start1234!' --no-admin http://localhost:8008
    done

{{- if or ( eq .Values.demomode.mode "defined" ) ( eq .Values.demomode.mode "federation" ) }}
---
apiVersion: batch/v1
kind: CronJob
metadata:
  name: {{ $name }}-save-job
  namespace: {{ .Release.Namespace }}
spec:
  schedule: "{{ .Values.demomode.interval | default "0 0 31 12 0" }}"
    {{- if not ( eq .Values.demomode.mode "defined" ) }}
  suspend: true # Schedule wird hiermit deaktiviert, schedule ist aber Pflicht.
    {{- else }}
  suspend: false
    {{- end }}
  concurrencyPolicy: Forbid
  jobTemplate:
    spec:
      ttlSecondsAfterFinished: 90
      template:
        metadata:
          labels:
            {{- include "matrix-synapse.labels" . | nindent 12 }}
            app.kubernetes.io/component: save-demo-cronjob
            to-apiserver: allowed
        spec:
        {{- include "matrix-synapse.imagePullSecrets" $ | nindent 14 }}
          serviceAccountName: {{ include "matrix-synapse.demomodeServiceAccountName" . }}
          initContainers:
            - name: scale-down
              image: "{{ printf "%s/%s:%s" .Values.signingkey.job.publishImage.registry .Values.signingkey.job.publishImage.repository (.Values.signingkey.job.publishImage.tag | default "latest") }}"
              command:
                - sh
                - -c
                - |
                  kubectl -n {{ .Release.Namespace }} scale deployment/{{ include "matrix-synapse.fullname" . }} --replicas=0
            - name: db-dump-container
              image: "{{ .Values.demomode.postgresqldump | default (printf "%s/%s:%s" .Values.postgresql.image.registry .Values.postgresql.image.repository .Values.postgresql.image.tag ) }}"
              env:
              {{- if and
                  ($postgresSecretName := include "matrix-synapse.postgresql.secret-name" $)
                  ($postgresSecretKey := include "matrix-synapse.postgresql.secret-key" $)
              }}
                - name: PGPASSWORD
                  valueFrom:
                    secretKeyRef:
                      name: {{ $postgresSecretName }}
                      key: {{ $postgresSecretKey }}
              {{- else }}
                - name: PGPASSWORD
                  value: {{ include "matrix-synapse.postgresql.password" . }}
              {{- end }}
                - name: POSTGRES_USER
                  value: {{ include "matrix-synapse.postgresql.username" . }}
                - name: POSTGRES_DB
                  value: {{ include "matrix-synapse.postgresql.database" . }}
                - name: DUMPDB
                  value: "{{ .Values.demomode.sqldump | default "dump.sql" }}"
              volumeMounts:
                - name: dump
                  mountPath: /opt
              command:
                - sh
                - -c
                - |
                  mkdir -p /opt/sql && \
                  PGPASSWORD=$PGPASSWORD && PGDB=$POSTGRES_DB && \
                  psql -h {{ include "matrix-synapse.postgresql.host" . }} -U $POSTGRES_USER -d $POSTGRES_DB -c 'truncate table devices, device_inbox, device_auth_providers, access_tokens, refresh_tokens,pushers cascade' && \
                  pg_dump -h {{ include "matrix-synapse.postgresql.host" . }} -U $POSTGRES_USER $POSTGRES_DB > /opt/sql/$DUMPDB
          containers:
            - name: save-synapse-media
              image: "{{ printf "%s/%s:%s" .Values.signingkey.job.publishImage.registry .Values.signingkey.job.publishImage.repository (.Values.signingkey.job.publishImage.tag | default "latest") }}"
              command:
                - sh
                - -c
                - |
                  kubectl -n {{ .Release.Namespace }} scale deployment/{{ include "matrix-synapse.fullname" . }} --replicas=1 && \
                  mkdir -p /opt/media && \
                  {{- if .Values.workers.media_repository.enabled }}
                  POD_NAME=$(kubectl get pods --namespace {{ .Release.Namespace }} -l "app.kubernetes.io/name={{ include "matrix-synapse.name" . }},app.kubernetes.io/instance={{ .Release.Name }},app.kubernetes.io/component=media-repository" -o jsonpath="{.items[0].metadata.name}") && \
                  rm -rf /opt/media/* && \
                  kubectl cp {{ $.Release.Namespace }}/$POD_NAME:/synapse/data/media /opt/media --no-preserve=false
                  {{- else }}
                  POD_NAME=$(kubectl get pods --namespace {{ .Release.Namespace }} -l "app.kubernetes.io/name={{ include "matrix-synapse.name" . }},app.kubernetes.io/instance={{ .Release.Name }},app.kubernetes.io/component=synapse" -o jsonpath="{.items[0].metadata.name}") && \
                  rm -rf /opt/media/* && \
                  kubectl cp {{ $.Release.Namespace }}/$POD_NAME:/synapse/data/media /opt/media --no-preserve=false
                  {{- end }}
              volumeMounts:
                - name: dump
                  mountPath: /opt
                - mountPath: /script
                  name: script
                  readOnly: true
          volumes:
            - name: dump
              persistentVolumeClaim:
                claimName: {{ .Values.demomode.existingClaim | default "demo-save" }}
            - name: script
              configMap:
                name: {{ $name }}-script
                defaultMode: 0755
          restartPolicy: Never
{{- end }}

---
apiVersion: batch/v1
kind: CronJob
metadata:
  name: {{ $name }}-reset-job
  namespace: {{ .Release.Namespace }}
spec:
  schedule: {{ .Values.demomode.interval | default "0 0 * * 0" | quote }}
  concurrencyPolicy: Forbid
  suspend: false
  jobTemplate:
    spec:
      ttlSecondsAfterFinished: 90
      template:
        metadata:
          labels:
            {{- include "matrix-synapse.labels" . | nindent 12 }}
            app.kubernetes.io/component: resetdemo-cronjob
            to-apiserver: allowed
        spec:
          {{- include "matrix-synapse.imagePullSecrets" $ | nindent 10 }}
          serviceAccountName: {{ include "matrix-synapse.demomodeServiceAccountName" . }}
          initContainers:
            - name: scale-down
              image: "{{ printf "%s/%s:%s" .Values.signingkey.job.publishImage.registry .Values.signingkey.job.publishImage.repository (.Values.signingkey.job.publishImage.tag | default "latest") }}"
              command:
                - sh
                - -c
                - kubectl -n {{ .Release.Namespace }} scale deployment/{{ include "matrix-synapse.fullname" . }} --replicas=0
            - name: reset-db-container
              image: "{{ .Values.demomode.postgresqldump | default (printf "%s/%s:%s" .Values.postgresql.image.registry .Values.postgresql.image.repository .Values.postgresql.image.tag ) }}"
              env:
              {{- if and
                  ($postgresSecretName := include "matrix-synapse.postgresql.secret-name" .)
                  ($postgresSecretKey := include "matrix-synapse.postgresql.secret-key" .)
              }}
                - name: PGPASSWORD
                  valueFrom:
                    secretKeyRef:
                      name: {{ $postgresSecretName }}
                      key: {{ $postgresSecretKey }}
              {{- else }}
                - name: PGPASSWORD
                  value: {{ include "matrix-synapse.postgresql.password" . }}
              {{- end }}
                - name: POSTGRES_USER
                  value: {{ include "matrix-synapse.postgresql.username" . }}
                - name: POSTGRES_DB
                  value: {{ include "matrix-synapse.postgresql.database" . }}
                {{- if eq .Values.demomode.mode "defined" }}
                - name: DUMPDB
                  value: "{{ .Values.demomode.sqldump | default "dump.sql" }}"
              volumeMounts:
                - name: dump
                  mountPath: /opt
                  readOnly: true
                {{- end }}
              securityContext:
                # runAsNonRoot: true
                readOnlyRootFilesystem: true
              command:
                - sh
                - -c
                - |
                  PGPASSWORD=$PGPASSWORD && PGDB=$POSTGRES_DB && \
                  dropdb -f -h {{ include "matrix-synapse.postgresql.host" . }} -U $POSTGRES_USER  $POSTGRES_DB --if-exists && \
                  createdb -h {{ include "matrix-synapse.postgresql.host" . }} -U $POSTGRES_USER  --encoding=UTF8 --locale=C --template=template0 --owner=$POSTGRES_USER $POSTGRES_DB && \
                  {{- if eq .Values.demomode.mode "defined" }}
                  cat /opt/sql/$DUMPDB | psql -h {{ include "matrix-synapse.postgresql.host" . }} -U $POSTGRES_USER $POSTGRES_DB && \
                  {{- end }}
                  echo "done"
          containers:
            - name: reset-synapse-rollout
              image: "{{ printf "%s/%s:%s" .Values.signingkey.job.publishImage.registry .Values.signingkey.job.publishImage.repository (.Values.signingkey.job.publishImage.tag | default "latest") }}"
              command:
                - sh
                - -c
                - |
                  kubectl -n {{ .Release.Namespace }} scale deployment/{{ include "matrix-synapse.fullname" . }} --replicas=1 && \
                  {{- range $worker, $config := .Values.workers }}
                    {{- if $config.enabled }}
                    {{- $name := $worker | replace "_" "-" }}
                      {{- if regexMatch $name "media-repository" }}
                        {{- if $.Values.persistence.enabled }}
                  POD_NAME=$(kubectl get pods --namespace  {{ $.Release.Namespace }} -l "app.kubernetes.io/name={{ include "matrix-synapse.name" $ }},app.kubernetes.io/instance={{ $.Release.Name }},app.kubernetes.io/component=media-repository" -o jsonpath="{.items[0].metadata.name}") && \
                  kubectl exec --namespace {{ $.Release.Namespace }} $POD_NAME -- /bin/sh -c "rm -rf /synapse/data/media/*" && \
                          {{- if eq $.Values.demomode.mode "defined" }}
                  kubectl cp /opt/media {{ $.Release.Namespace }}/$POD_NAME:/synapse/data/media && \
                          {{- end }}
                        {{- end }}
                      {{- end }}
                  kubectl -n {{ $.Release.Namespace }} rollout restart statefulset/{{ include "matrix-synapse.workername" (dict "global" $ "worker" $name) }} && \
                    {{- end }}
                  {{- end }}
                  /script/check-deployment.sh
              volumeMounts:
                - mountPath: /script
                  name: script
                  readOnly: true
                  {{- if eq .Values.demomode.mode "defined" }}
                - name: dump
                  mountPath: /opt
                  readOnly: true
                  {{- end }}
              securityContext:
                # runAsNonRoot: true
                readOnlyRootFilesystem: true
          volumes:
          {{- if eq .Values.demomode.mode "defined" }}
            - name: dump
              persistentVolumeClaim:
                claimName: {{ .Values.demomode.existingClaim | default "demo-save" }}
          {{- end }}
            - name: script
              configMap:
                name: {{ $name }}-script
                defaultMode: 0755
          restartPolicy: Never
{{- end -}}
