{{/* SPDX-FileCopyrightText: 2022–2025 BWI GmbH */}}
{{/* SPDX-License-Identifier: Apache-2.0 */}}
{{- if .Values.contentscanner.enabled }}
{{- $scannername := include "matrix-synapse.externalname" (dict "global" . "external" "schadcodescanner") -}}
{{- $name := include "matrix-synapse.externalname" (dict "global" . "external" "matrix-content-scanner") -}}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ $name }}
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "matrix-synapse.labels" . | nindent 4 }}
data:
  config.yaml: |
    # Configuration for hosting the HTTP(S) API.
    web:
        port: 8080
        {{- if .Values.ipv4Only }}
        host: "0.0.0.0"
        {{- else }}
        host: "::"
        {{- end }}

    scan:
    # The script to run to scan a file. This script will be called with a path to the
    # downloaded file as its only argument, e.g. "./example.sh /temp/foo.bar/my_file".
    # Required.
        script: '{{ .Values.contentscanner.scanScript }}'

        # The temporary directory to use
        temp_directory: '/tmp'

        # Command to run to remove files from disk once they have been scanned.
        # Optional, defaults to "rm".
        removal_command: "shred -u"

        # List of allowed MIME types. If a file has a MIME type that's not in this list, its
        # scan is considered failed.
        # Optional, defaults to allowing all MIME types.
        # allowed_mimetypes: ["image/jpeg"]
        
        # List of blocked MIME types.
        # If specified, `allowed_mimetypes` must not be specified as well.
        # If specified, a file whose MIME type is on this list will produce a scan that is
        # considered failed.
        # Unrecognised binary files are considered to be `application/octet-stream`.
        # Unrecognised text files are considered to be `text/plain`.
        # Optional.
        # blocked_mimetypes: ["image/jpeg"]

      {{- if ne nil .Values.contentscanner.allowed_mimetypes }}
        allowed_mimetypes: {{-
          .Values.contentscanner.allowed_mimetypes | toYaml | nindent 10
        }}
      {{- else if .Values.contentscanner.blocked_mimetypes }}
        blocked_mimetypes: {{-
          .Values.contentscanner.blocked_mimetypes | toYaml | nindent 10
        }}
      {{- end }}

    # Optional. A command to execute when unlinking files from the file
    # system instead of using node's fs.unlink. When executed, the first
    # argument to the command will be the path of the file being
    # removed.
    # Docker note: the `srm` utility is already bundled into the base Docker image
    # built by the project. See `Dockerfile` for additional notes.
    #
    # Each entry in the cache includes the result of the scan as well as a copy of the media
    # that was scanned. If the media fails the scan, however, or is larger than the configured
    # maximum size (if set), no copy of the media is stored in the result cache.
    result_cache:
        # List of exit codes from the scanning script that shouldn't cause the result of the
        # scan to be cached for future requests.
        # Optional, defaults to an empty list (i.e. results are cached regardless of the
        # script's exit code).
        exit_codes_to_ignore: [1, 2]

        # Maximum number of results that can be stored in the cache. If more files are
        # scanned before existing items reach their TTL, the least-recently accessed will be
        # evicted.
        # Optional, defaults to 1024.
        max_size: {{ .Values.contentscanner.max_cache_size | default 2048 }}

        # The maximum amount of time an entry will stay in the cache before being evicted.
        # Optional, defaults to 1 week.
        ttl: {{ .Values.contentscanner.ttl | default "1d" }}

        # The maximum cachable file size. If a file is bigger than this size, a copy of it
        # will be not be cached even if the scan succeeds. If the file is requested again, it
        # is downloaded again from the homeserver, but is not written to disk or scanned.
        # Optional, defaults to no maximum size.
        max_file_size: {{ .Values.contentscanner.max_file_size | default "100MB" }}


    # Configuration for downloading files.
    # When downloading files directly from their respective homeservers (which is the default
    # behaviour), the homeservers' default URLs are determined using .well-known discovery
    # (defaults to using the homeserver's domain if not available).
    # See https://spec.matrix.org/latest/client-server-api/#server-discovery for more info.
    # Settings in this section (apart from `base_homeserver_url`) apply to .well-known
    # discovery requests as well as file download ones.
    download:
        # If provided, all files are downloaded using the homeserver at this URL. If this
        # setting is provided, .well-known discovery is not used to determine the base URL
        # to use.
        # Optional, defaults to downloading files directly from their respective homeservers.
        {{- if .Values.workers.media_repository.enabled }}
        base_homeserver_url: 'http://{{ include "matrix-synapse.workername" (dict "global" . "worker" "media-repository") }}.{{ .Release.Namespace }}.svc:8083'
        {{- else }}
        base_homeserver_url: 'http://{{ include "matrix-synapse.fullname" . }}.{{ .Release.Namespace }}.svc:8008'
        {{- end }}

        # HTTP(S) proxy to use when sending requests.
        # Optional, defaults to no proxy.
        #proxy: "http://10.0.0.1:3128"

        # Headers to send in outgoing requests.
        # Optional, defaults to no additional headers.
        #additional_headers:
        #    user-agent: "matrix-content-scanner"

    # Configuration for decrypting Olm-encrypted request bodies.
    crypto:
        # The path to the Olm pickle file. This file contains the key pair to use when
        # encrypting and decrypting encrypted POST request bodies.
        # A new keypair will be created at startup if the pickle file doesn't already exist.
        # Required.
        pickle_path: "/crypto/pickle"

        # The key to use to decode the pickle file.
        # Required.
        pickle_key: {{ randAlphaNum 24 | nospace | quote }}

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ $name }}-scannerscript
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "matrix-synapse.labels" . | nindent 4 }}
data:
  scanner.sh: |
    #!/bin/bash
    # This is an example script to run when a file is scanned.
    # - ${1} will be the path to the file to scan.
    # - The exit code should be 0 for files that are deemed safe/clean.

    # c-icap-client beendet immer mit 0, mit und ohne Virusfund
    # Option "-v" for local logging
    c-icap-client -v -i {{ $scannername }} -p 1344 -f ${1} -s virus_scan -o ${1}-log &> ${1}-response;
    returncode=$?;

    # nur bei Virusfund, wird ein Log angelegt.
    # HTML Antwort enthält Virus-Informationen
    if [ -e ${1}-log ]; then
      # workaround zum 'warning: command substitution: ignored null byte in input'
      # entsteht durch html message von c-icap-service
      # remove all HTML tags and spaces
      sed -i -e 's/<[^>]*>//g;/^\s*$/d' ${1}-log
      # get 3rd and 4th row
      return=$(sed -n '3,4 p' ${1}-log | tr -d '\n' );
      rm ${1}-log;
    else
     # remove all spaces, and only 2nd row
      return=$(cat ${1}-response | sed '/^\s*$/d' | sed -n '2 p');
    fi

    result=$(echo $return | grep -c -i "virus" )
    echo "################# Result of Scan ${1}: $result; $return";

    # Virus wurde gefunden
    if [[ $result != 0 ]]; then
      # delete temporary files
      rm ${1}-log;
      rm ${1}-response;
      exit 3

    # exit 0=clean, wenn kein String "VIRUS" enthalten,
    # der returncode vom i-cap-client 0 ist und der return-log ist leer
    elif [[ $result == 0 ]] && [[ $returncode == 0 ]] ; then
      rm ${1}-response;
      exit 0

    # es stimmt was mit der Datei nicht (Größe oder sonstiges, so dass der returncode ungleich 0 ist)
    # oder die Rückgabe vom ClamAV ist nicht leer (Rückgabe andere als VIRUS FOUND)
    # Ergebnis wird nicht ge-chached (exit_codes_to_ignore), da dies ein unerwarteter Zustand ist
    elif [[ $returncode != 0 ]] ; then
      echo "################# Error: $(cat ${1}-response)"
      # delete temporary files
      if [ -e ${1}-log ]; then
        rm ${1}-log;
      fi
      rm ${1}-response;
      exit 1
    fi

  scanner_bypass.sh: |
    #!/bin/bash
    # This is an example script to run when a file is scanned.
    # Caution this script always returns 0 and bypasses the virus scanner.
    # Only use this if you know what you are doing.
    exit 0

{{- end }}
